package ioc;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class F {

	@Autowired
	private A a;
	
	public F() {
		System.out.println("dans le constructeur");
		System.out.println(a); // null
	}
	
	
	@PostConstruct
	public void init() {
		System.out.println("dans le init @PostConstruct");
		System.out.println(a); // non null
	}
}
