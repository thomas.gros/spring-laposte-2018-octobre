package ioc;

public class C {

	private D composantD;
	
	public C(D composantD) {
		this.composantD = composantD;
	}
	
	public D getComposantD() {
		return composantD;
	}
}
