package ioc;

public class D {
	
	private String data;

	public D(String data) {
		super();
		this.data = data;
	}
	
	public String getData() {
		return data;
	}
}
