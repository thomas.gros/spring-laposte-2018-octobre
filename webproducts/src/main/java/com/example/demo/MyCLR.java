package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyCLR implements CommandLineRunner {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductMongoRepository productMongoRepository;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("blah blah blah...");
		System.out.println(productRepository);
		
		System.out.println(productRepository.findAll());
		
		System.out.println(productRepository.count());
		
		System.out.println(productRepository.findById(1));
		
		
		System.out.println("NOUVEAU PRODUIT");
		Product p = new Product();
		p.setId(1);
		p.setTitle("fraises");
		p.setPrice(10);
		
		productRepository.save(p);
		
		System.out.println(productRepository.count());
		
		Product p1 = productRepository.findById(1).get();
		p1.setTitle("nouveau titre");
		productRepository.save(p1);
		
		
		System.out.println(productRepository.findByTitle("fraises"));

		ProductMongo pm = new ProductMongo();
		pm.setTitle("patates");
		pm.setPrice(20);
		productMongoRepository.save(pm);
		
		System.out.println(productMongoRepository.findAll());
		
	}
}
