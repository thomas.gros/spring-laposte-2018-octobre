package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProductTest {

	@Test
	public void idShouldBeNotNull() {
		
		Product p = new Product();

		p.setId(1);

		assertThat(p.getId()).isEqualTo(1);
	}

}
